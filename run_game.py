#!/usr/bin/env python
import sys, os

def main():
    #figure out our directories
    # Locate the present working directory
    LOCALPATH = os.getcwd()
    # Locate the base directory
    BASEPATH = os.path.split(os.path.abspath(sys.argv[0]))[0]
    # Get a path from first command-line argument
    if len(sys.argv) > 1:
        ARGPATH = os.path.abspath(sys.argv[1])
    else:
        ARGPATH = ''
    print("LOCALPATH: " + LOCALPATH)
    print("BASEPATH: " + BASEPATH)
    print("ARGPATH: " + ARGPATH)
    # Locate the directory containing game engine code
    testbase = os.path.join(BASEPATH, 'lib')
    if os.path.isdir(testbase):
        CODEPATH = os.path.join(testbase, '')
    # Locate the game directory
    testarg = os.path.join(ARGPATH, 'code')
    testlocal = os.path.join(LOCALPATH, 'lib')
    if os.path.isdir(testarg):
        GAMEPATH = os.path.join(testarg, '')
    elif os.path.isdir(testlocal):
        GAMEPATH = os.path.join(testlocal, '')
    else:
        # Default to same as game engine directory if all else fails
        GAMEPATH = CODEPATH
    # Locate the game data directory
    testarg = os.path.join(ARGPATH, 'data')
    testlocal = os.path.join(LOCALPATH, 'data')
    if ARGPATH != '' and os.path.isdir(testarg):
        DATAPATH = os.path.join(testarg, '')
    elif os.path.isdir(testlocal):
        DATAPATH = os.path.join(testlocal, '')
    else:
        # Default to same as game engine directory if all else fails
        DATAPATH = CODEPATH
    print("CODEPATH: " + CODEPATH)
    print("GAMEPATH: " + GAMEPATH)
    print("DATAPATH: " + DATAPATH)

    #apply our directories and test environment
#    os.chdir(DATADIR)
    sys.path.insert(0, CODEPATH)
    sys.path.insert(0, GAMEPATH)

    #run game and protect from exceptions
    try:
        from ebigo import mainloop as main
        import Main as game
        main.main(sys.argv, DATAPATH, game)
        print('Main program ended...')
    except KeyboardInterrupt:
        print('Keyboard Interrupt (Control-C)...')

if __name__ == '__main__':
    main()
    sys.exit()
