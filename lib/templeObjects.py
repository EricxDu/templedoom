# Copyright 2017, 2018 Eric Duhamel

# This file is part of Temple of Doom.

# Temple of Doom is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Temple of Doom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Temple of Doom.  If not, see <http://www.gnu.org/licenses/>.

import pygame  # TODO: this dependency should only be in EBIGO

from ebigo import gameobjects

class Game(gameobjects.PlayObject):
    def __init__(self):
        self.width = 320
        self.height = 240
        super(Game, self).__init__(self.width, self.height)
        openBoxes = (
            0, 7, 16, 25, 34, 55, 56, 57, 58, 77, 78, 79, 87, 88
        )  # these are tiles that can be traversed
        self.blockBox = []
        for i in range(90):
            if i not in openBoxes:
                self.blockBox.append(i)
        self.players = []
        self.monsters = []
        self.missiles = []

    def addMonsters(self, number, spawnrect):
        for i in range(number):
            x = self.getNumber(spawnrect.x+spawnrect.width, spawnrect.x)
            y = self.getNumber(spawnrect.y+spawnrect.height, spawnrect.y)
            monster = self.addObject(Monster('slime', x, y, self.scale),
                self.playField)
            self.monsters.append(monster)

    def addPlayer(self, name, number, spawnrect):
        spawnP = (
            spawnrect.midleft,
            spawnrect.midtop,
            spawnrect.midright,
            spawnrect.midbottom,
        )
        x = spawnP[number][0]
        y = spawnP[number][1]
        char = self.addObject(
            Player(name, x, y, self.scale),
            self.playField.PBOUNDRECT,
            number
        )
        char.center = spawnP[number]
        missile = self.addObject(
            Missile(),
            self.playField,
        )
        char.addMissile(missile)
        self.missiles.append(missile)
        self.players.append(char)

    def doCombat(self):
        for monster in self.monsters:
            for missile in self.missiles:
                if (not missile.killed and
                        monster.colliderect(missile)):
                    missile.destroy()
                    monster.destroy()
            if monster.killed:
                self.monsters.remove(monster)


class Missile(gameobjects.Missile):
    def __init__(self):
        super(Missile, self).__init__(0, 0, 8, 8)
        self.name = 'bolt'
        self.edginess = self.STOP

    def hitBlock(self):
        self.destroy()

    def hitBound(self):
        self.destroy()


class Character(gameobjects.Shooter):
    def __init__(self, name, x, y, scale=1):
        width = 12*scale
        height = 13*scale
        super(Character, self).__init__(name, x, y, width, height)

    def doDirection(self, direction, speed):
        direct = direction
        if direction not in self.ANYORTHOGONAL:
            if direction in self.ANYLEFT:
                direct = self.LEFT
            if direction in self.ANYRIGHT:
                direct = self.RIGHT
        # TODO: this is a HACK assuming the playfield grid originates
        # at 0,0
        size = 32
        pathRect = pygame.Rect((0, 0, size, size))
        pathRect.x = round((self.centerx-pathRect.x)/pathRect.width)*pathRect.width
        pathRect.y = round((self.centery-pathRect.y)/pathRect.height)*pathRect.height
        direct, speed = self.getCenterPath(
            pathRect, direct, speed)
        super(Character, self).doDirection(direct, speed)


class Monster(Character):
    def __init__(self, name, x, y, scale=1):
        super(Monster, self).__init__(name, x, y, scale)
        self.moving = 0
        self.edginess = self.STOP

    def wander(self):
        """Move randomly

        Stop each move after some time."""
        moveChance = 2
        moveCounter = 16
        if self.moving > 0 and self.moving < moveCounter:
            self.doDirection(self.direction, self.speed)
            self.moving += 1
        else:
            if self.getChance(100, moveChance):
                self.direction = self.getChoice(self.ANYORTHOGONAL)
                self.moving = 1
            self.takeStep()


class Player(Character):
    def __init__(self, name, x, y, scale=1):
        super(Player, self).__init__(name, x, y, scale)
        self.maxsteps = 12
        self.speed = 2*scale
        self.missile = None
        self.exitRoom = False
        self.edginess = self.WRAP

    def fireMissile(self):
        if not self.missile.fired:
            x = self.centerx
            y = self.centery
            direction = self.direction
            speed = 8
            super(Character, self).fireMissile(x, y, direction, speed)

    def doAction(self, index):
        if index == 1:
            self.fireMissile()

    def hitBound(self):
        self.exitRoom = True


class Room(gameobjects.FieldObject):
    def __init__(self, scale=1):
        x = 0
        y = 0
        width = 272*scale
        height = 208*scale
        super(Room, self).__init__(x, y, width, height)
        self.PBOUNDRECT = self.copy()
        self.PBOUNDRECT.width = 228*scale
        self.PBOUNDRECT.height = 184*scale
        self.PBOUNDRECT.center = self.center
        self.MBOUNDRECT = self.copy()
        self.MBOUNDRECT.width = 208*scale
        self.MBOUNDRECT.height = 144*scale
        self.MBOUNDRECT.center = self.center
        # Create a map of terrain for this room
        boxMap = [
            [1,2,5,5,5,5,5,6,7,8,9,9,9,9,9,3,4],
            [10,11,14,14,14,14,14,15,16,17,18,18,18,18,18,12,13],
            [37,38,78,79,78,79,78,79,77,78,79,78,79,78,79,39,40],
            [37,38,87,88,87,88,87,88,77,87,88,87,88,87,88,39,40],
            [37,38,78,79,78,79,78,79,77,78,79,78,79,78,79,39,40],
            [46,47,87,88,87,88,87,88,77,87,88,87,88,87,88,48,49],
            [55,56,77,77,77,77,77,77,77,77,77,77,77,77,77,57,58],
            [64,65,78,79,78,79,78,79,77,78,79,78,79,78,79,66,67],
            [73,74,87,88,87,88,87,88,77,87,88,87,88,87,88,75,76],
            [73,74,78,79,78,79,78,79,77,78,79,78,79,78,79,75,76],
            [73,74,87,88,87,88,87,88,77,87,88,87,88,87,88,75,76],
            [19,20,23,23,23,23,23,24,25,26,27,27,27,27,27,21,22],
            [28,29,32,32,32,32,32,33,34,35,36,36,36,36,36,30,31]
        ]
        # add all the boxmaps
        boxW = 16*scale
        boxH = 16*scale
        self.addBoxMap('dungeon2', boxMap, boxW, boxH)
