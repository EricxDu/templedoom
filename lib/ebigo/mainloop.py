# Copyright 2016, 2017 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import pygame, sys, os
from pygame.locals import *

import graphics
#import controls

import Main

#DEFAULTNAME = "Game Window"
#DEFAULTCOLOR = (0, 0, 0)
#DEFAULTFPS = 32
DEBUGPARAM = '--debug'

def main(args, datadir, metagame):
    # Initialize pygame
    pygame.init()
    # Figure out the screen size
    for resolution in graphics.DISPLAYRESOLUTIONS:
        if resolution in args:
            displayWidth = graphics.DISPLAYRESOLUTIONS[resolution][0]
            displayHeight = graphics.DISPLAYRESOLUTIONS[resolution][1]
        else:
            displayWidth = graphics.VGA[0]
            displayHeight = graphics.VGA[1]
    print("Resolution: " + str(displayWidth) + "x" + str(displayHeight))
    # TODO: graphics initialization is now in metagame so find a way to
    # send the following options there
    # Determine how verbose graphics.py is
    if DEBUGPARAM in args:
        debugMode = True
    else:
        debugMode = False
    # Initialize graphics
    if graphics.FULLSCREENPARAM in args:
        SCREENMODE = graphics.FULLSCREEN
    else:
        SCREENMODE = graphics.WINDOWED
    # Initialize the game module
    meta = Main.Meta(displayWidth, displayHeight)
    meta.loadAssets(datadir)
    # Start the game loop
    done = False
    while not done:
        done = meta.run()
    pygame.quit()

if __name__ == '__main__':
    main(sys.argv)
