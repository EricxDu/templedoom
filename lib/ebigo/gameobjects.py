# Copyright 2015, 2016, 2017, 2018 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import pygame, random, copy, time, math
random.seed()

# TODO: make these bitwise integers based on Wang Blob tile system
NORTH = 1 # 0001
EAST = 2 # 0010
SOUTH = 4 # 0100
WEST = 8 # 1000
OO = 0 # 0000
NN = 1 # 0001
EE = 2 # 0010
NE = 3 # 0011
SS = 4 # 0100
SN = 5 # 0101
NS = 5 # 0101
SE = 6 # 0110
WW = 8 # 1000
NW = 9 # 1001
WE = 10 # 1010
SW = 12 # 1100

# New way of keeping track of walls
WALLSDICT = {NORTH: (NN, NE, SN, NW),
             EAST: (EE, NE, SE, WE),
             SOUTH: (SS, SN, SE, SW),
             WEST: (WW, NW, WE, SW)}

DEFAULTTILEMAP = [[ 0, 0, 0, 0 ],
                  [ 0, 0, 0, 0 ],
                  [ 0, 0, 0, 0 ],
                  [ 0, 0, 0, 0 ]]

class GameObject(pygame.Rect):
    def __init__(self, rect):
        self.x = rect[0]
        self.y = rect[1]
        self.width = rect[2]
        self.height = rect[3]
        self.DIRECTLEFT = 360
        self.DIRECTUP = 270
        self.DIRECTRIGHT = 180
        self.DIRECTDOWN = 90
        self.DELTALEFT = (-1, 0)
        self.DELTAUPLEFT = (-1, -1)
        self.DELTAUP = (0, -1)
        self.DELTAUPRIGHT = (1, -1)
        self.DELTARIGHT = (1, 0)
        self.DELTADOWNRIGHT = (1, 1)
        self.DELTADOWN = (0, 1)
        self.DELTADOWNLEFT = (-1, 1)
        self.DELTANONE = (0, 0)
        self.LEFT = 360
        self.UPLEFT = 315
        self.UP = 270
        self.UPRIGHT = 225
        self.RIGHT = 180
        self.DOWNRIGHT = 135
        self.DOWN = 90
        self.DOWNLEFT = 45
        self.NODIRECT = 0
        self.CLOCKDIR1 = 240
        self.CLOCKDIR2 = 210
        self.CLOCKDIR3 = 180
        self.CLOCKDIR4 = 150
        self.CLOCKDIR5 = 120
        self.CLOCKDIR6 = 90
        self.CLOCKDIR7 = 60
        self.CLOCKDIR8 = 30
        self.CLOCKDIR9 = 360
        self.CLOCKDIR10 = 330
        self.CLOCKDIR11 = 300
        self.CLOCKDIR12 = 270
        self.ANYLEFT =  (self.LEFT, self.UPLEFT, self.DOWNLEFT)
        self.ANYUP =    (self.UP, self.UPLEFT, self.UPRIGHT)
        self.ANYDOWN =  (self.DOWN, self.DOWNLEFT, self.DOWNRIGHT)
        self.ANYRIGHT = (self.RIGHT, self.UPRIGHT, self.DOWNRIGHT)
        self.ANYDIAGONAL = (self.UPRIGHT, self.DOWNRIGHT,
                            self.DOWNLEFT, self.UPLEFT)
        self.DIRECT2ANY = {
            self.LEFT: self.ANYLEFT,
            self.UP: self.ANYUP,
            self.DOWN: self.ANYDOWN,
            self.RIGHT: self.ANYRIGHT,
            self.NODIRECT: self.ANYLEFT
        }
        self.DIAGONAL2DIRECTS = {
            self.UPLEFT: (self.UP, self.LEFT),
            self.UPRIGHT: (self.UP, self.RIGHT),
            self.DOWNRIGHT: (self.DOWN, self.RIGHT),
            self.DOWNLEFT: (self.DOWN, self.LEFT),
        }
        self.DIAG2Y = {
            self.UPLEFT: self.UP,
            self.UPRIGHT: self.UP,
            self.DOWNRIGHT: self.DOWN,
            self.DOWNLEFT: self.DOWN,
        }
        self.DIAG2X = {
            self.UPLEFT: self.LEFT,
            self.UPRIGHT: self.RIGHT,
            self.DOWNRIGHT: self.RIGHT,
            self.DOWNLEFT: self.LEFT,
        }
        self.ANYORTHOGONAL = (self.LEFT, self.UP, self.DOWN, self.RIGHT)
        self.ANYHORIZONTAL = (self.LEFT, self.RIGHT)
        self.ANYVERTICAL = (self.UP, self.DOWN)
        self.ANYDIRECT = (
            self.LEFT,
            self.UPLEFT,
            self.UP,
            self.UPRIGHT,
            self.RIGHT,
            self.DOWNRIGHT,
            self.DOWN,
            self.DOWNLEFT,
        )
        self.ANYDELTA = (
            self.DELTALEFT,
            self.DELTAUPLEFT,
            self.DELTAUP,
            self.DELTAUPRIGHT,
            self.DELTARIGHT,
            self.DELTADOWNRIGHT,
            self.DELTADOWN,
            self.DELTADOWNLEFT,
            self.DELTANONE
        )
        self.DIRECT2DELTA = {
            self.LEFT: self.DELTALEFT,
            self.UPLEFT: self.DELTAUPLEFT,
            self.UP: self.DELTAUP,
            self.UPRIGHT: self.DELTAUPRIGHT,
            self.RIGHT: self.DELTARIGHT,
            self.DOWNRIGHT: self.DELTADOWNRIGHT,
            self.DOWN: self.DELTADOWN,
            self.DOWNLEFT: self.DELTADOWNLEFT
        }
        self.DELTA2DIRECT = {
            self.DELTALEFT: self.LEFT,
            self.DELTAUPLEFT: self.UPLEFT,
            self.DELTAUP: self.UP,
            self.DELTAUPRIGHT: self.UPRIGHT,
            self.DELTARIGHT: self.RIGHT,
            self.DELTADOWNRIGHT: self.DOWNRIGHT,
            self.DELTADOWN: self.DOWN,
            self.DELTADOWNLEFT: self.DOWNLEFT,
            self.DELTANONE: self.NODIRECT
        }
        self.DIRECT2OPPOSITE = {
            self.LEFT: self.RIGHT,
            self.UPLEFT: self.DOWNRIGHT,
            self.UP: self.DOWN,
            self.UPRIGHT: self.DOWNLEFT,
            self.RIGHT: self.LEFT,
            self.DOWNRIGHT: self.UPLEFT,
            self.DOWN: self.UP,
            self.DOWNLEFT: self.UPRIGHT,
            self.NODIRECT: None
        }
        self.DIRECT2VERBAL = {
            self.LEFT: 'left',
            self.UP: 'up',
            self.DOWN: 'down',
            self.RIGHT: 'right'
        }

    def getChoice(self, choices):
        return random.choice(choices)

    def getChance(self, denominator, numerator):
        """Return whether you beat the odds.

        Enter the odds as See-Threepio tells them to you, i.e. 3720 to
        1. Remember the denominator is the larger number, and the
        numerator is the smaller. They are reversed above from the
        normal order compared to when you are typing out a fraction."""
        # Generate a random number as high as the larger number
        randomNum = random.randint(0, denominator)
        # Return True if the number is less than the smaller number
        # i.e. you beat the odds!
        return randomNum < numerator

    def getNumber(self, high, low=0):
        return random.randint(low, high)


class PawnObject(GameObject):
    def __init__(self, name, x, y, width, height):
        self.name = name
        self.type = name
        self.step = 0
        self.maxsteps = 16
        self.resetstep = 1
        self.stopstep = 0
        self.speed = 4
        self.status = "None"
        self.states = [{'name': "None", 'size': width, 'steps': 1},]
        self.xparams = {}
        super(PawnObject, self).__init__((x, y, width, height))
        self.direction = 0
        self.dx = 0
        self.dy = 0
        self.extraStats = {}
        self.TOPLEFT = 'top-left'
        self.MIDTOP = 'middle-top'
        self.TOPRIGHT = 'top-right'
        self.MIDRIGHT = 'middle-right'
        self.BOTTOMRIGHT = 'bottom-right'
        self.MIDBOTTOM = 'middle-bottom'
        self.BOTTOMLEFT = 'bottom-left'
        self.MIDLEFT = 'middle-left'
        self.MIDDLE = 'middle'
        self.anchor = self.TOPLEFT
        # Some constants that define playfield edge behavior
        self.WRAP = 'wrap'
        self.BOUNCE = 'bounce'
        self.STOP = 'stop'
        self.SLIDE = 'slide'
        self.edginess = self.WRAP
        self.killed = False

    def doDirection(self, direction, speed=4):
        if direction in self.ANYDIRECT:
            self.setDirection(direction)
            self.moveDirection(direction, speed)
            self.takeStep()

    def doAction(self, index):
        """Perform an action based on a player intention

        Intentions are numbered 1 to an indefinite maximum"""
        return False

    def setDirection(self, direction):
        if direction in self.ANYDIRECT:
            self.direction = direction

    def moveDirection(self, direction, distance=1):
        if direction in self.DIRECT2DELTA:
            (deltaX, deltaY) = self.DIRECT2DELTA[direction]
            deltaX *= distance
            deltaY *= distance
            self.moveDelta(deltaX, deltaY)

    def moveDelta(self, dx, dy):
        self.x += dx
        self.y += dy

    def move(self):
        self.moveDelta(self.dx, self.dy)

    def hitBound(self):
        return False

    def hitBlock(self):
        return False

    def setAnchor(self, anchorname):
        self.anchor = anchorname
        if anchorname == self.MIDBOTTOM:
            self.midbottom = self.topleft

    def setDeltas(self, direction, speed):
        newSine = math.sin(math.radians(direction))
        newCosine = math.cos(math.radians(direction))
        if newSine <= 1:
            self.dy = (newSine*speed)
        else:
            self.dy = 0
        if newCosine <= 1:
            self.dx = (-newCosine*speed)
        else:
            self.dx = 0

    def addDeltas(self, delta, maxdelta=None):
        self.dx += delta[DELTAX]
        self.dy += delta[DELTAY]
        # Constrain each delta within a maximum
        if maxdelta != None:
            if self.dx > maxdelta:
                self.dx = maxdelta
            elif self.dx < -maxdelta:
                self.dx = -maxdelta
            if self.dy > maxdelta:
                self.dy = maxdelta
            elif self.dy < -maxdelta:
                self.dy = -maxdelta
        return (self.dx, self.dy)

    def getDeltas(self):
        return (self.dx, self.dy)

    def setDirectionByDeltas(self, deltas):
        self.direction = getDirectionByDeltas(deltas)
        return self.direction

    # TODO: can rotate clockwise or counterclockwise; rename
    def rotateClockwise(self, degrees=1):
        if self.direction in self.DELTA2DIRECT:
            self.direction = 0
        self.direction += degrees
        if self.direction > 360:
            self.direction = 1
        if self.direction < 1:
            self.direction = 360

    # DEPRECATED
    def advanceStep(self, maxsteps):
        self.step += 1
        if self.step > maxsteps:
            self.step = 0

    def takeStep(self, steps=1):
        self.step += steps
        if self.step > self.maxsteps:
            self.resetStep()

    def resetStep(self):
        self.step = 1

    def getStatus(self, cat='_'):
        """Return a dictionary describing the state of this object."""
        Dict = {
            'name': self.name + cat + str(self.status),
            'x': self.x,
            'y': self.y,
            'step': self.step,
            'direction': self.direction
        }
        return Dict

    def getDict(self):
        dict = {}
        dict['name'] = self.name
        dict['type'] = self.type
        dict['x'] = self.x
        dict['y'] = self.y
        dict['status'] = self.status
        dict['step'] = self.step
        dict['direction'] = self.direction
        return dict

    def getStats(self):
        """Return a dictionary describing the properties of this object."""
        Dict = {
            'name': self.name,
            'width': self.width,
            'height': self.height,
            'steps': self.maxsteps
        }
        return Dict

    def getRect(self):
        return pygame.Rect(self.x, self.y, self.width, self.height)

    def getExtraStats(self):
        # TODO: why do we need this?
        return self.extraStats

    def moveOutsideBlocks(self, blocklist):
        for block in blocklist:
            self.moveOutside(block, block.direction)
            return True
        return None

    def moveOutside(self, rect, direction=None):
        if direction == self.LEFT:
            self.right = rect.left-1
        elif direction == self.UP:
#            self.bottom = rect.top-1
            self.bottom = rect.top
        elif direction == self.RIGHT:
            self.left = rect.right
        elif direction == self.DOWN:
            self.top = rect.bottom
        else:
            # TODO: the following is too specific, abstractify it
            if self.colliderect(rect):
                if self.dx != 0:
                    backX = -self.dx
                else:
                    backX = 0
                if not rect.collidepoint(self.centerx+backX, self.centery):
                    self.moveDirection(backX, 0)
        return None

    def moveInside(self, rect, method=None):
        """Move inside the provided area

        Several methods of moving are defined. Moves in place
        because returning coordinates proved too complicated. Return
        None or a direction describing how the object moved."""
        if not rect.colliderect(self):
            if method in (None, self.WRAP):
                if self.centerx < rect.x:
                    self.centerx = (rect.x + rect.width)
                    return self.RIGHT
                elif self.centery < rect.y:
                    self.centery = (rect.y + rect.height)
                    return self.DOWN
                elif self.centery > (rect.y + rect.height):
                    self.centery = rect.y
                    return self.UP
                elif self.centerx > (rect.x + rect.width):
                    self.centerx = rect.x
                    return self.LEFT
            elif method == self.STOP:
                # TODO: also move back inside the Rect
                self.dx = 0
                self.dy = 0
                return True
        else:
            return None

    def getCenterPath(self, rect, direction, distance):
        """Return the direction and distance toward the center of a Rect."""
        newRect = pygame.Rect(copy.copy(self))
        newDirect = direction
        newDist = distance
        if direction == self.UP:
            if self.centerx > rect.centerx:
                if self.centerx - rect.centerx > distance:
                    newDirect = self.LEFT
                    newDist = distance
                else:
                    newDirect = self.LEFT
                    newDist = (self.centerx - rect.centerx)
            elif self.centerx < rect.centerx:
                if rect.centerx - self.centerx > distance:
                    newDirect = self.RIGHT
                    newDist = distance
                else:
                    newDirect = self.RIGHT
                    newDist = (rect.centerx - self.centerx)
            else:
                newDirect = self.UP
                newDist = distance
        elif direction == self.LEFT:
            if self.centery > rect.centery:
                if self.centery - rect.centery > distance:
                    newDirect = self.UP
                    newDist = distance
                else:
                    newDirect = self.UP
                    newDist = (self.centery - rect.centery)
            elif self.centery < rect.centery:
                if rect.centery - self.centery > distance:
                    newDirect = self.DOWN
                    newDist = distance
                else:
                    newDirect = self.DOWN
                    newDist = (rect.centery - self.centery)
            else:
                newDirect = self.LEFT
                newDist = distance
        elif direction == self.DOWN:
            if self.centerx > rect.centerx:
                if self.centerx - rect.centerx > distance:
                    newDirect = self.LEFT
                    newDist = distance
                else:
                    newDirect = self.LEFT
                    newDist = (self.centerx - rect.centerx)
            elif self.centerx < rect.centerx:
                if rect.centerx - self.centerx > distance:
                    newDirect = self.RIGHT
                    newDist = distance
                else:
                    newDirect = self.RIGHT
                    newDist = (rect.centerx - self.centerx)
            else:
                newDirect = self.DOWN
                newDist = distance
        elif direction == self.RIGHT:
            if self.centery > rect.centery:
                if self.centery - rect.centery > distance:
                    newDirect = self.UP
                    newDist = distance
                else:
                    newDirect = self.UP
                    newDist = (self.centery - rect.centery)
            elif self.centery < rect.centery:
                if rect.centery - self.centery > distance:
                    newDirect = self.DOWN
                    newDist = distance
                else:
                    newDirect = self.DOWN
                    newDist = (rect.centery - self.centery)
            else:
                newDirect = self.RIGHT
                newDist = distance
        return newDirect, newDist

    def destroy(self):
        self.killed = True


class Block(pygame.Rect):
    def __init__(self, rect):
        """Represents a Rect that blocks objects from moving."""
        self.direction = None
        super(Block, self).__init__(rect)


class FieldObject(GameObject):
    def __init__(self, x, y, width, height, tilemap=None):
#        self.name = name
        if tilemap:
            self.columns = len(tilemap[0])
            self.rows = len(tilemap)
        else:
            self.columns = 1
            self.rows = 1
        self.TILEWIDTH = width / self.columns
        self.TILEHEIGHT = height / self.rows
        self.TILEMAP = tilemap
        self.boxmaps = []
        super(FieldObject, self).__init__((x, y, width, height))

    def getRect(self):
        return pygame.Rect((self.x, self.y, self.width, self.height))

    def addBoxMap(self, name, map, tilewidth, tileheight):
        self.columns = len(map[0])
        self.rows = len(map)
        self.TILEWIDTH = self.width / self.columns
        self.TILEHEIGHT = self.height / self.rows
        field = pygame.Rect((
            self.x,
            self.y,
            tilewidth*self.columns,
            tileheight*self.rows
        ))
        mapdict = {
            'name': name,
            'map': map,
            'field': field,
            'tilewidth': tilewidth,
            'tileheight': tileheight
        }
        self.boxmaps.append(mapdict)

    def getAllBoxMaps(self):
        return self.boxmaps

    def getBlocksAtRect(self, rect, blocklist=None, direction=None, layer=0):
        blocks = []
        if rect.direction in self.ANYDIAGONAL:
            directs = self.DIAGONAL2DIRECTS[rect.direction]
        else:
            directs = (rect.direction,)
        for direct in directs:
            anyDirects = self.DIRECT2ANY[direct]
            for anyDirect in anyDirects:
                boxRect, boxValue = self.getBoxAtRect(
                    rect, anyDirect, layer
                )
                if boxValue in blocklist:
                    block = Block(boxRect)
                    block.direction = self.DIRECT2OPPOSITE[direct]
                    # TODO: also omit any blocks that share a colliding
                    # edge with the first one
                    if block not in blocks:
                        blocks.append(block)
        return blocks

    def getBoxAtRect(self, rect, direction=None, index=0):
        """Return info about box located at supplied rect.

        Can also specify whether its at a certain edge of rect."""
        if direction == self.LEFT:
            return self.getBoxAtPixel(rect.left, rect.centery, index)
        elif direction == self.UPLEFT:
            return self.getBoxAtPixel(rect.left, rect.top, index)
        elif direction == self.UP:
            return self.getBoxAtPixel(rect.centerx, rect.top, index)
        elif direction == self.UPRIGHT:
            return self.getBoxAtPixel(rect.right, rect.top, index)
        elif direction == self.RIGHT:
            return self.getBoxAtPixel(rect.right, rect.centery, index)
        elif direction == self.DOWNRIGHT:
            return self.getBoxAtPixel(rect.right, rect.bottom, index)
        elif direction == self.DOWN:
            return self.getBoxAtPixel(rect.centerx, rect.bottom, index)
        else:
            return self.getBoxAtPixel(rect.centerx, rect.centery, index)

    def getBoxAtPixel(self, x, y, index=0):
        boxwidth = self.TILEWIDTH
        boxheight = self.TILEHEIGHT
        cellX = int((x - self.x) / boxwidth)
        cellY = int((y - self.y) / boxheight)
        boxX = self.x + (cellX * boxwidth)
        boxY = self.y + (cellY * boxheight)
        boxRect = pygame.Rect((boxX, boxY, boxwidth, boxheight))
        if len(self.boxmaps) > index:
            boxmap = self.boxmaps[index]['map']
        else:
            boxmap = self.boxmaps[0]
        if (cellX > -1 and cellY > -1
                and cellX < len(boxmap[0])
                and cellY < len(boxmap)):
            boxValue = boxmap[int(cellY)][int(cellX)]
        else:
            boxValue = 0
        return boxRect, boxValue

    def createMap(self, columns, rows, floodvalue):
        boxMap = []
        for boxH in range(int(rows)):
            boxMapRow = []
            for boxW in range(int(columns)):
                boxMapRow.append(floodvalue)
            boxMap.append(boxMapRow)
        return boxMap

    def decorateMap(self, plainmap, plainval):
        # Set up an array of choices so that common decorations are randomly
        # chosen more often than rare decorations
        boxChoices = []
        decor = {11: 48, 18: 16, 17: 2, 16: 1}
        for d in decor:
            for i in range(decor[d]):
                boxChoices.append(d)
        # Replace boxes of map with decorations
        decoratedMap = copy.copy(plainmap)
        for i in range(len(plainmap)):
            for box in range(len(plainmap[i])):
                if plainmap[i][box] == plainval:
                    decoratedMap[i][box] = self.getChoice(tuple(boxChoices))
        return decoratedMap

    def addFeature(self, boxmap, farray, x, y):
        i = 0
        # Complicated while loop to paste the pattern in farray
        # at the position described by x, y
        while y+i < len(boxmap) and i < len(farray):
            j = 0
            while x+j < len(boxmap[y]) and j < len(farray[i]):
                boxmap[y+i][x+j] = farray[i][j]
                j += 1
            i += 1
        return boxmap


class PlayObject(GameObject):
    def __init__(self, width=0, height=0):
        super(PlayObject, self).__init__((0, 0, width, height))
#        self.setScale(width, height)
        self.scale = 1
        self.allObjs = []
        self.allFieldObjs = []
        self.WRAP = 'wrap'
        self.BOUNCE = 'bounce'
        self.STOP = 'stop'
        self.SLIDE = 'slide'
        self.timers = []
        self.NOEVENT = 0
        return None

    def getScale(self, windowwidth, windowheight):
        if self.width == None:
            self.width = windowwidth
        if self.height == None:
            self.height = windowheight
        # Determine scale based on difference
        if windowwidth > 0 and windowheight > 0:
            if windowheight < (self.height*2):
                scale = 1
            elif windowheight < (self.height*3):
                scale = 2
            else:
                scale = 3
        else:
            scale = 1
        return scale

    # TODO: setScale is a duplicate of getScale; deprecate
    def setScale(self, wwidth, wheight):
        if self.width == 0:
            self.width = wwidth
        if self.height == 0:
            self.height = wheight
        # Determine scale based on difference
        if wwidth > 0 and wheight > 0:
            if wheight < (self.height*2):
                self.scale = 1
            elif wheight < (self.height*3):
                self.scale = 2
            else:
                self.scale = 3
        else:
            self.scale = 1

    def addObject(self, object, field=None, index=None):
        object.boundary = field
        object.field = field
        if index is None:
            self.allObjs.append(object)
        else:
            self.allObjs.insert(index, object)
        return object

    def removeObject(self, object):
        self.allObjs.remove(object)

    def countObjects(self, name):
        count = 0
        for object in self.allObjs:
            if object.name == name:
                count += 1
        return count

    def removeAllObjects(self, name=None):
        """Remove all objects with a certain name from the list."""
        if name != None:
            for i in range(len(self.allObjs) - 1, -1, -1):
                object = self.allObjs[i]
                if object.name == name:
                    del self.allObjs[i]

    def getAllObjs(self):
        return self.allObjs

    def getAllObjDicts(self):
        allObjDicts = []
        for obj in self.allObjs:
            allObjDicts.append(obj.getDict())
        return allObjDicts

    def getAllObjStates(self):
        allObjStates = []
        for obj in self.allObjs:
            for objState in obj.states:
                allObjStates.append(
                    {'name': obj.name + '_' + objState['name'],
                     'size': objState['size'],
                     'steps': objState['steps']})
        return allObjStates

    def getAllObjNames(self):
        allObjNames = []
        for obj in self.allObjs:
            allObjNames.append(obj.getStats())
#                {'name': obj.fullname,
#                {'name': obj.name,
#                 'size': obj.height,
#                 'steps': obj.steps}
#            )
        return allObjNames

    def addFieldObject(self, fieldobject):
        self.allFieldObjs.append(fieldobject)
        return fieldobject

    def getField(self, index=0):
        if len(self.allFieldObjs) > index:
            return self.allFieldObjs[index]
        else:
            return None

    def getNextField(self, curfieldnum, direction=0, width=0):
        fieldNum = curfieldnum
        lastField = len(self.allFieldObjs) - 1
        if direction in (self.LEFT,):
            fieldNum = curfieldnum - 1
            if fieldNum < 0:
                fieldNum = lastField
        elif direction in (self.UP,):
            fieldNum = curfieldnum - width
            # If direction goes off the top it will wrap to the bottom
            if fieldNum < 0:
                fieldNum = lastField + fieldNum
        elif direction in (self.DOWN,):
            fieldNum = curfieldnum + width
            # If direction goes off the bottom it will wrap to the top
            if fieldNum > lastField:
                fieldNum = fieldNum - lastField
        elif direction in (self.RIGHT,):
            fieldNum = curfieldnum + 1
            if fieldNum > lastField:
                fieldNum = 0
        else:
            fieldNum += 1
            if fieldNum > lastField:
                fieldNum = 0
#        print(fieldNum, lastField)
        return fieldNum

    def getAllFieldObjs(self):
        """Get all the play fields in the game.

        This is named similarly to getAllObjs(), but is used more like
        getAllObjStates(). Should fix that to be more clear."""
        allDicts = []
        for field in self.allFieldObjs:
            dict = {}
#            dict['name'] = field.name
            dict['maps'] = field.boxmaps
            dict['width'] = field.width
            dict['height'] = field.height
            dict['tilewidth'] = field.TILEWIDTH
            dict['tileheight'] = field.TILEHEIGHT
            allDicts.append(dict)
        return allDicts

    def setTimer(self):
        self.timers.append(time.time())
        return len(self.timers)-1

    def getTimer(self, index):
        if len(self.timers) > index:
            return time.time() - self.timers[index]
        else:
            return None

    def resetTimer(self, index):
        if len(self.timers) > index:
            self.timers[index] = time.time()
            return True
        else:
            return False

    def getOrthogonal(self, diagonal, direction):
#        orthogonal = list(diagonal)
        if diagonal in self.ANYDIAGONAL:
            orthogonal = diagonal - 45
            if orthogonal < 1:
                orthogonal = 360
#            if direction in self.ANYLEFT or direction in self.ANYRIGHT:
#                orthogonal[DELTAX] = 0
#            elif direction in self.ANYUP or direction in self.ANYDOWN:
#                orthogonal[DELTAY] = 0
        else:
            orthogonal = diagonal
        return orthogonal

    def getRandomPoint(self, field):
        if field == None:
            field = pygame.Rect(0, 0, self.width, self.height)
        x = random.randint(
            field.x,
            field.x+field.width
        )
        y = random.randint(
            field.y,
            field.y+field.height
        )
        return x, y

    def playGame(self, controldicts, edgebehavior=None,
                 boundrect=None, xparams={}):
        """Perform a step of game play with default generic physics."""
        for playerNum, object in enumerate(self.allObjs):
            if len(controldicts) > playerNum:
                stickDelta = controldicts[playerNum]['stick']
                if controldicts[playerNum]['button']:
                    object.doAction(1)
            else:
                stickDelta = None
            # Move objects based on player input
            if stickDelta in self.DELTA2DIRECT:
#                print(stickDelta, self.DELTA2DIRECT)
                direct = object.DELTA2DIRECT[tuple(stickDelta)]
                speed = object.speed
                if direct in self.ANYDIRECT:
                    object.doDirection(direct, speed)
            # Or just move objects based on their deltas
            if object.dx != 0 or object.dy != 0:
                if not object.killed:
                    object.move()
            # Handle whether the object exceeded the playfield boundary
            if object.field == None:
                # boundary is a rectangle the size of the game
                boundRect = pygame.Rect((0, 0, self.width, self.height))
            else:
                # boundary is determined by the object
                boundRect = object.field
            # Use the boundary rectangle that was just determined
            if object.moveInside(boundRect, object.edginess) != None:
                # Let object know it hit game boundary
                object.hitBound()

    def getObjectStats(self):
        """Return a dictionary describing objects that will be in the game."""
        objDicts = []
#        print(self.allObjs)
#        object = GameObject("someobject", 0, 0, 1, 1)
        for object in self.allObjs:
            objDicts.append(object.getStats())
        return objDicts


class WallMazeSprite(PawnObject):
    def __init__(self, name, dimensions):
        self.dead = False
        self.step = 0
        self.DEATHDELAY = 10
        self.deathAnimation = self.DEATHDELAY
        super(WallMazeSprite, self).__init__(name, dimensions[0], dimensions[1], dimensions[2], dimensions[3])

#    def move(self, direction, distance):
#        if direction == UP:
#            self.y = self.y - distance
#        elif direction == LEFT:
#            self.x = self.x - distance
#        elif direction == DOWN:
#            self.y = self.y + distance
#        elif direction == RIGHT:
#            self.x = self.x + distance

    def getBlockedPath(self, rect, direction, distance):
        """Return the distance to center of rect."""
        newDist = distance
        if direction == self.UP:
            if self.centery - rect.centery > distance:
                newDist = distance
            else:
                newDist = (self.centery - rect.centery)
        elif direction == self.LEFT:
            if self.centerx - rect.centerx > distance:
                newDist = distance
            else:
                newDist = (self.centerx - rect.centerx)
        elif direction == self.DOWN:
            if rect.centery - self.centery > distance:
                newDist = distance
            else:
                newDist = (rect.centery - self.centery)
        elif direction == self.RIGHT:
            if rect.centerx - self.centerx > distance:
                newDist = distance
            else:
                newDist = (rect.centerx - self.centerx)
        return newDist

    # Labyrinth-specific: move to Labyrinth and make this generic
    def followPath(self, wallmaze, maxDistance,
                   currentDirection, intentDirection, wallsdict):
        distanceToPath = wallmaze.getDistanceToPath(intentDirection, self)
#        pathIsOpen = wallmaze.getPathIsOpen(intentDirection, self, wallsdict)
        pathIsOpen = wallmaze.getPathIsOpen(intentDirection, self)
        # To travel the center of the path, we find the
        # distance to the center of the path in the indicated
        # direction, and move toward there. Walls block movement.
        if distanceToPath == 0 and pathIsOpen:
            return intentDirection, maxDistance
        elif distanceToPath <= maxDistance:
            return currentDirection, distanceToPath
        else:
            return currentDirection, maxDistance


class Missile(PawnObject):
    def __init__(self, x, y, width, height):
        super(Missile, self).__init__('missile', x, y, width, height)
        self.x = x
        self.y = y
        self.dx = 0
        self.dy = 0
        self.width = width
        self.height = height
        self.step = 0
        self.maxsteps = 4
        self.shrinkW = 0
        self.shrinkH = 0
        self.fired = False
        self.killed = True

    def fire(self, x, y, direction, speed):
        self.center = (x, y)
        self.setDeltas(direction, speed)
        self.direction = direction
        self.fired = True
        self.killed = False

    def move(self):
        super(Missile, self).move()
        self.takeStep()

    def destroy(self):
        self.killed = True
        self.fired = False


#class Shooter(WallMazeSprite):
class Shooter(PawnObject):
    counter = 0
    owner = 0
    fired = False

    def __init__(self, name, x, y, width, height):
        super(Shooter, self).__init__(name, x, y, width, height)
        self.dx = 0
        self.dy = 0
        self.missile = Missile(0, 0, 1, 1)

    def addMissile(self, missile):
        self.missile = missile

    def fireMissile(self, x, y, direction, speed):
        self.missile.fire(x, y, direction, speed)

    def moveMissile(self, direction, distance):
        if direction == self.UP:
            self.missile.y = self.missile.y - distance
        elif direction == self.LEFT:
            self.missile.x = self.missile.x - distance
        elif direction == self.DOWN:
            self.missile.y = self.missile.y + distance
        elif direction == self.RIGHT:
            self.missile.x = self.missile.x + distance

    def stopMissile(self):
        self.missile.fired = False

    def targetInRange(self, rect, target):
        if (self.direction == self.LEFT and
            self.centery > target.top and
            self.centery < target.bottom and
            target.x < self.x or
            self.direction == self.UP and
            self.centerx > target.left and
            self.centerx < target.right and
            target.y < self.y or
            self.direction == self.DOWN and
            self.centerx > target.left and
            self.centerx < target.right and
            target.y > self.y or
            self.direction == self.RIGHT and
            self.centery > target.top and
            self.centery < target.bottom and
            target.x > self.x):
            # If target is lined up vertically or horizontally,
            # and in our sights, we can fire
            return True
        else:
            return False


class WallMazeCell(pygame.Rect):
    def __init__(self, x, y, width, height, walls):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.walls = walls

    def getWall(self, direction, wallsdict):
#        if direction == 360:
#            direction = WEST
#        if direction == 270:
#            direction = NORTH
#        if direction == 180:
#            direction = EAST
#        if direction == 90:
#            direction = SOUTH
#        if self.walls > 15:
#            self.cleanWalls = self.walls - 15
#        else:
#            self.cleanWalls = self.walls
#        if self.walls & direction != 0:
#            return True
#        else:
#            return False
        if self.walls in wallsdict[direction]:
            return True
        else:
            return False

    def getWalls(self):
        return self.walls

    def getRect(self):
        return pygame.Rect((self.x, self.y, self.width, self.height))

    def getCoords(self):
        return self.mapX, self.mapY

    def getDist(self, target):
        distX = int(abs(self.x - target.x) / self.width)
        distY = int(abs(self.y - target.y) / self.height)
        return distX + distY


class WallMaze(FieldObject):
    def __init__(self, dimensions, wallmap):
        x = dimensions[0]
        y = dimensions[1]
        width = dimensions[2]
        height = dimensions[3]
        super(WallMaze, self).__init__('none', x, y, width, height)
        self.columns = len(wallmap[0])
        self.rows = len(wallmap)
        self.CELLWIDTH = self.width / self.columns
        self.CELLHEIGHT = self.height / self.rows
        self.wallmap = wallmap

    def set_wallmap(self, wallmap):
        self.columns = len(wallmap[0])
        self.rows = len(wallmap)
        self.CELLWIDTH = self.width / self.columns
        self.CELLHEIGHT = self.height / self.rows
        self.wallmap = wallmap

#    def getCellRect(self, x, y):
#        xCell = (x - self.x) / self.CELLWIDTH
#        yCell = (y - self.y) / self.CELLHEIGHT
#        cellX = (xCell * self.CELLWIDTH) + self.x
#        cellY = (yCell * self.CELLHEIGHT) + self.y
#        return pygame.Rect(cellX, cellY, self.CELLWIDTH, self.CELLHEIGHT)

    def getBoxAtPixel(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = (x - self.x) / cellwidth
        ycell = (y - self.y) / cellheight
        return xcell, ycell

    def getLeftTopOfCell(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        cell_x = self.x + (cellwidth * x)
        cell_y = self.y + (cellheight * y)
        return (cell_x, cell_y)

    def getCenterOfBox(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        cell_x = self.x + ((cellwidth * x) + (cellwidth / 2))
        cell_y = self.y + ((cellheight * y) + (cellwidth / 2))
        return (cell_x, cell_y)

    def getCell(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = int((x - self.x) / cellwidth)
        ycell = int((y - self.y) / cellheight)
        cell_x = (xcell * cellwidth) + self.x
        cell_y = (ycell * cellheight) + self.y
        if xcell < len(self.wallmap[0]) and xcell > -1:
            if ycell < len(self.wallmap) and ycell > -1:
                return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, self.wallmap[ycell][xcell])
        return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, 0)

    def getNextBox(self, x, y, direction):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = int((x - self.x) / cellwidth)
        ycell = int((y - self.y) / cellheight)
        if direction == self.UP:
            ycell -= 1
        elif direction == self.LEFT:
            xcell -= 1
        elif direction == self.DOWN:
            ycell += 1
        elif direction == self.RIGHT:
            xcell += 1
        cell_x = (xcell * cellwidth) + self.x
        cell_y = (ycell * cellheight) + self.y
        if xcell < len(self.wallmap[0]) and xcell > -1:
            if ycell < len(self.wallmap) and ycell > -1:
                return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, self.wallmap[ycell][xcell])
        return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, 0)

    def getDistanceToPath(self, pathDirection, objRect):
        cellRect = self.getCellRect(objRect.centerx, objRect.centery)
        moveRect = pygame.Rect(objRect.x, objRect.y, cellRect.width, cellRect.height)
        moveRect.centerx = objRect.centerx
        moveRect.centery = objRect.centery
        distanceToPath = 0
        if pathDirection == self.UP:
            if moveRect.left >= cellRect.left and moveRect.right <= cellRect.right:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.left - cellRect.left)
        if pathDirection == self.LEFT:
            if moveRect.top >= cellRect.top and moveRect.bottom <= cellRect.bottom:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.top - cellRect.top)
        if pathDirection == self.DOWN:
            if moveRect.left >= cellRect.left and moveRect.right <= cellRect.right:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.left - cellRect.left)
        if pathDirection == self.RIGHT:
            if moveRect.top >= cellRect.top and moveRect.bottom <= cellRect.bottom:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.top - cellRect.top)
        return distanceToPath

#    def getPathIsOpen(self, pathDirection, objRect, wallsdict):
#        cellRect = self.get_cellRect(objRect.centerx, objRect.centery)
#        cellWalls = self.getCellWalls(objRect.centerx, objRect.centery)
#        moveRect = pygame.Rect(objRect.x, objRect.y, cellRect.width, cellRect.height)
#        moveRect.centerx = objRect.centerx
#        moveRect.centery = objRect.centery
#        pathIsOpen = True
#        if pathDirection == UP and cellWalls.getWalls() in wallsdict[NORTH]:
#            if moveRect.top <= cellRect.top:
#                pathIsOpen = False
#        if pathDirection == LEFT and cellWalls.getWalls() in wallsdict[WEST]:
#            if moveRect.left <= cellRect.left:
#                pathIsOpen = False
#        if pathDirection == DOWN and cellWalls.getWalls() in wallsdict[SOUTH]:
#            if moveRect.bottom >= cellRect.bottom:
#                pathIsOpen = False
#        if pathDirection == RIGHT and cellWalls.getWalls() in wallsdict[EAST]:
#            if moveRect.right >= cellRect.right:
#                pathIsOpen = False
#        return pathIsOpen


def getDirectionByDeltas(deltas):
    dx = deltas[DELTAX]
    dy = deltas[DELTAY]
    totalDelta = abs(dx) + abs(dy)
    threshDelta = totalDelta / 3
    if dx > 0 and dx > threshDelta:
        dirX = 1
    elif dx < 0 and dx < -threshDelta:
        dirX = -1
    else:
        dirX = 0
    if dy > 0 and dy > threshDelta:
        dirY = 1
    elif dy < 0 and dy < -threshDelta:
        dirY = -1
    else:
        dirY = 0
    return (dirX, dirY)

def getDeltasFromSpeed(deltas, speed):
    if speed > 0:
        dx = deltas[DELTAX]*speed
        dy = deltas[DELTAY]*speed
    return (dx, dy)

import unittest

class TestGameObjects(unittest.TestCase):
    def setUp(self):
        self.screenW = 50
        self.screenH = 50
        self.cellRect = pygame.Rect(50, 50, 12, 12)
        self.sprite = WallMazeSprite("Test Sprite", (0, 0, 8, 8))
        self.sprite.center = self.cellRect.topleft
        self.startX = self.sprite.x
        self.startY = self.sprite.y

    def test_getCenterPath(self):
        for dist in range(0, 16):
            for x in range(0, self.screenW):
                for y in range(0, self.screenH):
                    for direct in (ANYORTHOGONAL):
                        self.sprite.topleft = (x, y)
                        newDirect, newDist = self.sprite.getCenterPath(
                            self.cellRect, direct, dist)
                        self.sprite.move(newDirect, newDist)
                        if newDirect == direct:
                            if dir in (LEFT, RIGHT):
                                self.assertEqual(self.sprite.centery, self.cellRect.centery)
                            elif dir in (UP, DOWN):
                                self.assertEqual(self.sprite.centerx, self.cellRect.centerx)
                        # TODO: add check to make sure sprite doesn't move past
                        # center of rect

if __name__ == '__main__':
    unittest.main()
