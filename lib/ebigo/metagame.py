# Copyright 2016, 2017 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import pygame, os
import controls, graphics, sounds

DISPLAYWIDTH = graphics.VGA[0]
DISPLAYHEIGHT = graphics.VGA[1]
DEFAULTNAME = "Sample Game"
SCREENMODE = graphics.WINDOWED

class MetaGame(object):
    def __init__(self, game, winwidth, winheight):
        self.game = game
        # TODO: pass displaywidth/height and other variables to this
        # init instead of using static variables
        self.screenMan = graphics.Graphics(
            winwidth, winheight,
            DEFAULTNAME, SCREENMODE, False
        )
        self.playerInt = controls.ControlMap()
        #self.audioMan = sounds.Sounds()
        self.addAllObjects()
        self.redraw = True
        self.FPS = 30
        self.FPSCLOCK = pygame.time.Clock()
        print("Game Size: ", game.width, game.height)
        print("Game Scale: ", game.scale)

    def loadAssets(self, datadir):
        scale = self.game.scale
        self.addSpriteSheets(datadir)
        self.addAllTileSets(datadir, scale)

    def setGame(self, game):
        self.game = game

    def getGame(self):
        return self.game

    def run(self):
#        if self.game != None:
#            self.game.playGame()
        if self.playerInt.process_events() == -1:
            done = True
        else:
            done = False
        # Get player input from controls
        controlDicts = self.playerInt.getDicts()
        # Run the game
        # NOTE: any non-zero return represents a game state change
        # that needs to be addressed. This includes adding and
        # removing objects, changing the tilemap, etc.
        redrawEvent = self.game.playGame(controlDicts)
        if redrawEvent != None:
            self.redraw = True
        # Draw all the graphics
        if self.redraw:
            # draw the playing field if necessary
            if len(self.game.getAllFieldObjs()):
                self.screenMan.blank()
                field = self.game.getField()
                if field != None:
#                    print(field.getAllBoxMaps())
#                    print(len(field.getAllBoxMaps()))
                    for tilemap in field.getAllBoxMaps():
                        self.screenMan.drawTileMap(
                            tilemap['name'], tilemap['field'],
                            tilemap['map'],
                            graphics.BACKGROUND
                        )
            # add and remove any objects that were changed
            self.addAllObjects()
            # set flag to False and wait for next event
            self.redraw = False
        self.screenMan.updateSprites()
        self.screenMan.updateScreen()
        # Limit frames per second
        self.takeTime(self.FPS)
        return done

    def takeTime(self, framesPerSecond):
        self.FPSCLOCK.tick(framesPerSecond)

    def getSpriteSets(self):
        """Return names of all sprites that may be used in game.

        This is so the images can all be read from disk into memory.
        Games should override this method in order to return custom
        dictionaries representing the actual sprite sheets used."""
        if self.game != None:
            return self.game.getAllObjNames()
        else:
            return None

    def addSpriteSheets(self, datadir):
        allObjNames = self.getSpriteSets()
        for gameObj in allObjNames:
            name = gameObj['name']
            xparams = {}
            for param in gameObj:
                xparams[param] = gameObj[param]
            self.screenMan.addSpriteSheet(
                os.path.join(datadir, gameObj['name'] + '.png'),
                gameObj['name'],
                gameObj['width'],
                gameObj['height'],
                gameObj['steps'], self.game.scale, xparams
            )

    def getTileSets(self):
        if self.game != None:
            return self.game.getAllFieldObjs()
        else:
            return (None)

    def addAllTileSets(self, datadir, scale=1):
        """Tile-sets are used to draw foregrounds and backgrounds."""
        xparams = {'scale': scale,}
        allTileNames = self.getTileSets()
        for fieldObj in allTileNames:
            name = fieldObj['name']
            self.screenMan.addTileSet(
                datadir + name + '.png', name,
                fieldObj['tilewidth'],
                fieldObj['tileheight'],
                xparams
            )
            boxmaps = fieldObj['maps']
            if len(boxmaps) > 0:
                for boxmap in boxmaps:
                    self.screenMan.addTileSet(
                        datadir + boxmap['name'] + '.png',
                        boxmap['name'],
                        fieldObj['tilewidth'],
                        fieldObj['tileheight'],
                        xparams
                    )

    def getObjects(self):
        """Return the current state of ALL objects in game.

        This is needed in order to re/draw all the sprites."""
        if self.game != None:
            return self.game.getAllObjDicts()
        else:
            return None

    def addAllObjects(self):
        if self.game != None:
            self.screenMan.removeAllImages()
            for object in self.game.getAllObjs():
                self.screenMan.addAnimatedImage(object)
        return None

    def getMaps(self):
        """Return all of the currently used box-maps.

        Box-maps are used to build composite backgrounds and
        foregrounds using images from tile-sets."""
        return None
