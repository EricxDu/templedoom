# Copyright 2015, 2016, 2017, 2018 Eric Duhamel

# This file is part of EBIGO.

# EBIGO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# EBIGO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with EBIGO.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import time
import copy
import os

# RGBI: 0, 85, 170, 255
# Invent: 0,  64, 128, 192, 255
# OFF=0, NORMAL=170, INTENSE=+85
# OFF=0, LOW=64, NORMAL=128, INTENSE=+128
AQUA     = (  0, 255, 255)
BLACK    = (  0,   0,   0)
BLUE     = (  0,   0, 255)
FUCHSIA  = (255,   0, 255)
GRAY     = (128, 128, 128)
GREEN    = (  0, 255,   0)
LIME     = (  0, 255,   0)
MAROON   = (128,   0,   0)
NAVYBLUE = (  0,   0, 128)
OLIVE    = (128, 128,   0)
PURPLE   = (128,   0, 128)
RED      = (255,   0,   0)
SILVER   = (192, 192, 192)
TEAL     = (  0, 128, 128)
WHITE    = (255, 255, 255)
YELLOW   = (255, 255,   0)

FULLSCREENPARAM = '--fullscreen'
WINDOWEDPARAM = '--windowed'
DEBUGPARAM = '--debug'

FULLSCREEN = pygame.FULLSCREEN
WINDOWED = 0

DEFAULT = 'default'
#FONTNAME = 'FreeSansBold.ttf'
FONTNAME = None
SMALLFONTSIZE = 14
NORMALFONTSIZE = 18
NORMALGAP = 4
LARGEFONTSIZE = 48
OVERFONTSIZE = 72
FONTSIZE = NORMALFONTSIZE
NORMALFONTCOLOR = WHITE
DARKFONTCOLOR = GRAY

ALIGNMENT = "alignment"
# These global constants are needed for Graphics() and ImageSprite()
# to initialize
TOPLEFT = 'topleft'
BOTTOMLEFT = 'bottomleft'
TOPRIGHT = 'topright'
BOTTOMRIGHT = 'bottomright'
MIDTOP = 'midtop'
MIDLEFT = 'midleft'
MIDBOTTOM = 'midbottom'
MIDRIGHT = 'midright'
CENTERED = 'centered'
CENTER = 'center'
BACKGROUND = False
FOREGROUND = True

# The following are some notes on legacy graphics technology
# Default display resolutions
CGA = (320, 200)  # Supported 4 colors from a palette of 16 (4-bit)
EGA = (640, 350)  # Supported 16 colors from a palette of 64 (6-bit)
# Video Graphics Array
QVGA = (320, 240)
VGA = (640, 480)  # Supported 16 colors from a pallet of 256 (8-bit)
SVGA = (800, 600)
# Extended Graphics Array
XGA = (1024, 768)
WXGA = (1360, 768)
# High-Definition
HD = (1280, 720)  # HD is 3x QVGA in height, but does not fit the width
                  # of the 1024x768 desktop standard nor the width of
                  # 3x QVGA

DISPLAYRESOLUTIONS = {
    'CGA': CGA, 'EGA': EGA, 'VGA': VGA,
    'QVGA': QVGA, 'SVGA': SVGA,
    'XGA': XGA, 'WXGA': WXGA,
    'HD': HD}

SCALE = 'scale'
SCALE2 = True
DEBUGMODE = True

class Graphics():
    def __init__(self, displaywidth, displayheight, displaycaption,
                 fullscreen=False, debugmode=False):
        self.WINDOWWIDTH = displaywidth
        self.WINDOWHEIGHT = displayheight
        self.HALFWIDTH = displaywidth/2
        self.HALFHEIGHT = displayheight/2
        self.TOPOFSCREEN = 0  # 20
        self.BOTTOMOFSCREEN = displayheight  # 460
        self.LEFTOFSCREEN = 0  # 10
        self.RIGHTOFSCREEN = displaywidth  # 620
#        try:
#            iconImage = pygame.image.load(displayicon)
#            pygame.display.set_icon(iconImage)
#        except:
#            print "Display icon filname not found"
        pygame.display.set_caption(displaycaption)
        if fullscreen:
            flags=FULLSCREEN
        else:
            flags=0
        self.DISPLAYSURF = pygame.display.set_mode(
            (self.WINDOWWIDTH, self.WINDOWHEIGHT), flags
        )
        self.FONTSIZE = FONTSIZE
        self.NORMALFONTSIZE = NORMALFONTSIZE
        self.FONTNAME = FONTNAME
        self.BACKGROUND = BACKGROUND
        self.BACKGROUNDSURF = self.DISPLAYSURF.copy()
        self.FOREGROUND = FOREGROUND
        self.FOREGROUNDSURF = pygame.Surface([displaywidth, displayheight], pygame.SRCALPHA, 32)
        self.TILESETS = {}
        self.SPRITEIMAGES = {}
        self.dirtyRects = []
        self.oldRects = []
        if debugmode:
            self.DEBUGMODE = True
        else:
            self.DEBUGMODE = False
        self.AQUA = (  0, 255, 255)
        self.BLACK    = (  0,   0,   0)
        self.BLUE     = (  0,   0, 255)
        self.FUCHSIA  = (255,   0, 255)
        self.GRAY     = (128, 128, 128)
        self.GREEN    = (  0, 255,   0)
        self.LIME     = (  0, 255,   0)
        self.MAROON   = (128,   0,   0)
        self.NAVYBLUE = (  0,   0, 128)
        self.OLIVE    = (128, 128,   0)
        self.PURPLE   = (128,   0, 128)
        self.RED      = (255,   0,   0)
        self.SILVER   = (192, 192, 192)
        self.TEAL     = (  0, 128, 128)
        self.WHITE    = (255, 255, 255)
        self.YELLOW   = (255, 255,   0)
        self.TOPLEFT = TOPLEFT
        self.BOTTOMLEFT = BOTTOMLEFT
        self.TOPRIGHT = TOPRIGHT
        self.BOTTOMRIGHT = BOTTOMRIGHT
        self.MIDTOP = MIDTOP
        self.MIDLEFT = MIDLEFT
        self.MIDBOTTOM = MIDBOTTOM
        self.MIDRIGHT = MIDRIGHT
        self.CENTER = CENTER
#        self.dirtySprites = pygame.sprite.RenderUpdates()
        self.dirtySprites = pygame.sprite.LayeredDirty()
#        self.dirtyImages = pygame.sprite.RenderUpdates()
        self.dirtyImages = pygame.sprite.LayeredDirty()
        self.blank()

    def addSpriteSheet(self, filename, name, width, height, totalsteps,
                       scale=1, xparams={}):
        try:
            spriteImage = pygame.image.load(filename)
            if 'colorkey' in xparams:
                spriteImage.set_colorkey(xparams['colorkey'])
            else:
                transColor = spriteImage.get_at((0,0))
                spriteImage.set_colorkey(transColor)
            imgWidth = spriteImage.get_width()
            imgHeight = spriteImage.get_height()
            if scale > 1:
                spriteImage = pygame.transform.scale(
                    spriteImage, (imgWidth*scale, imgHeight*scale))
            clipwidth = width*scale
            clipheight = height*scale
            newSprite = {'image': spriteImage,
                         'clipwidth': clipwidth,
                         'clipheight': clipheight,
                         'clipsteps': totalsteps}
            for xparam in xparams:
                newSprite[xparam] = xparams[xparam]
#            self.spriteUpdates.add(sprite)
        except:
            font = pygame.font.SysFont('Calibri', 12, True, False)
            textImage = font.render(name, True, WHITE)
            image = pygame.Surface((width, height))
            pygame.draw.rect(image, WHITE, (0, 0, image.get_width(),
                             image.get_height()), 1)
            image.blit(
                textImage, (0, 0, textImage.get_width(), textImage.get_height())
            )
            newSprite = {'image': image,
                         'clipwidth': image.get_width(),
                         'clipheight': image.get_height(),
                         'clipsteps': 1}
            print("File not loaded: " + filename)
        imgWidth = newSprite['image'].get_width()
        if imgWidth < width:
            numFrames = 1
        else:
            numFrames = imgWidth / width
#        print name, imgWidth, clipSize, numFrames
        if totalsteps > 0:
            stepsPerFrame = totalsteps / numFrames
        else:
            stepsPerFrame = 1
#        newSprite['clipsteps'] = stepsPerFrame
        if self.DEBUGMODE:
            print("name: " + name)
            print("filename: " + filename)
            print("newSprite: " + str(newSprite))
        self.SPRITEIMAGES[name] = newSprite

    def addAnimatedImage(self, object, xparams={}):
        x = object.x
        y = object.y
        width = object.width
        height = object.height
        image = pygame.Surface((width, height))
        rect = pygame.Rect((x, y, width, height))
        pygame.draw.rect(image, BLUE, (0, 0, width, height), 1)
        sprite = AnimatedImage(rect, image)
        sprite.setObject(object)
        sprite.xparams = object.xparams  # TODO: a gameobject shouldn't
                                         # hold graphics parameters.
        self.dirtySprites.add(sprite)

    def removeAllImages(self):
        self.dirtySprites.empty()

    def updateSprites(self):
        # TODO: change the sprite's image based on the animation frame
        # Call update() on each sprite in dirstSprites
        # update() moves the rectangle of the sprite according to the
        # position of the represented object
        self.dirtySprites.update(self.SPRITEIMAGES)
        # Change the layer of each sprite according to Y value
        for sprite in self.dirtySprites:
            layer = sprite.rect.y
            self.dirtySprites.change_layer(sprite, layer)
        # Erase the image for each sprite
        self.dirtySprites.clear(self.DISPLAYSURF, self.BACKGROUNDSURF)
        # Draw each sprite in the new position
        spriteList = self.dirtySprites.draw(self.DISPLAYSURF)
        # Add all the rectangles to dirtyRects for updateScreen()
        for spriteRect in spriteList:
            self.dirtyRects.append(spriteRect)

    def addTileSet(self, filename, name, tileWidth, tileHeight,
                   xparams={'blank': None,}):
        if 'overlapheight' in xparams:
            overlapHeight = xparams['overlapheight']
        else:
            overlapHeight = 0
        try:
            image = pygame.image.load(filename)
        except:
            print("Filename not found: " + filename)
            image = pygame.Surface((tileWidth, tileHeight))
            pygame.draw.rect(image, WHITE, (0, 0, tileWidth, tileHeight))
        if 'scale' in xparams and xparams['scale'] > 0:
            scale = xparams['scale']
            imageW = image.get_width()
            imageH = image.get_height()
            image = pygame.transform.scale(image, (imageW*scale, imageH*scale))
            tileWidth *= scale
            tileHeight *= scale
        newTileSet = {'image': image,
                      'clipwidth': tileWidth,
                      'clipheight': tileHeight,
                      'overlapheight': overlapHeight}
        self.TILESETS[name] = newTileSet
#        self.TILESET = pygame.image.load(filename)
        self.TILEWIDTH = tileWidth
        self.TILEHEIGHT = tileHeight

    def drawTileMap(self, normalTiles, mapRect, tileMap, foreground=False, extHeight=0):
        #print(tileWidth, tileHeight, mapRect.width, mapRect.height)
        if normalTiles in self.TILESETS:
            imagesDict = self.TILESETS[normalTiles]
        else:
            font = pygame.font.SysFont('Calibri', 12, True, False)
            image = font.render('name missing', True, WHITE)
            print("name missing: " + normalTiles)
            imagesDict = {'image': image,
                          'clipwidth': image.get_width(),
                          'clipheight': image.get_height()}
        tileWidth = mapRect.width / len(tileMap[0])
        tileHeight = mapRect.height / len(tileMap)
        clipWidth = imagesDict['clipwidth']
        clipHeight = imagesDict['clipheight']
        #print("imagesDict:", imagesDict)
        tilesImage = imagesDict['image'].copy()
        tileColumns = tilesImage.get_width()/clipWidth
        tileRows = tilesImage.get_height()/clipHeight
        # This loop hangs if the tilesize is too big
        # Better fix that
        for cy in range(len(tileMap)):
            for cx in range(len(tileMap[cy])):
                tileNum = tileMap[cy][cx]
                cellRect = pygame.Rect(mapRect.x + (tileWidth * cx), mapRect.y + (tileHeight * cy), tileWidth, tileHeight+extHeight)
                tileNum -= 1
                tx = tileNum
                ty = 0
                # TODO: this loop hangs sometimes
                while tx > tileColumns-1:
                    tx -= tileColumns
                    ty += 1
                if (tx*clipWidth)+clipWidth-1 < tilesImage.get_width() and (ty*clipHeight)+clipHeight-1 < tilesImage.get_height():
                    # Clip the tile from the tilemap image and draw it
#                    print(normalTiles, tileNum)
                    if tileNum > -1:
                        wallImage = tilesImage.subsurface(
                            tx*clipWidth, ty*clipHeight,
                            clipWidth, clipHeight+extHeight
                        )
                        self.DISPLAYSURF.blit(wallImage, cellRect)
                        if foreground:
                            self.FOREGROUNDSURF.blit(wallImage, cellRect)
                        else:
                            self.BACKGROUNDSURF.blit(wallImage, cellRect)
                else:
#                    print(imagesDict)
#                    print("clipWH:", str(clipWidth), str(clipHeight))
#                    print("txty:", str(tx), str(ty))
#                    print(
#                        "Cliprect exceeded tilesheet",
#                        normalTiles, tileNum,
#                        tx*clipWidth, ty*clipHeight
#                    )
#                    print(clipWidth, clipHeight)
#                    print(tx, ty)
                    wallImage = tilesImage.subsurface(0, 0, clipWidth, clipHeight)
                    self.DISPLAYSURF.blit(wallImage, cellRect)
                    pygame.draw.rect(self.DISPLAYSURF, WHITE, mapRect, 1)
                    pygame.draw.rect(self.DISPLAYSURF, WHITE, cellRect, 1)
        mapRect.height += extHeight
        self.addDirtyRect(mapRect)

    def drawAllSprites(self, spriteDicts):
        # TODO: rename "sprites" because we are going to call them
        # "images"
        drawNewSprites = pygame.sprite.Group()
        for spriteDict in spriteDicts:
            spriteName = spriteDict['name']
            spriteX = spriteDict['x']
            spriteY = spriteDict['y']
            # Find the image based on object name
            if spriteName in self.SPRITEIMAGES:
                imagesDict = self.SPRITEIMAGES[spriteName]
                spriteSheet = imagesDict['image']
                spriteWidth = imagesDict['clipwidth']
                spriteHeight = imagesDict['clipheight']
            # Create a dummy image if not found
            else:
                spriteWidth = 8
                spriteHeight = 8
                spriteSheet = pygame.Surface(
                    (spriteWidth, spriteHeight)
                )
                pygame.draw.rect(
                    spriteSheet, WHITE,
                    (0, 0, spriteWidth, spriteHeight),
                    1
                )
            # Now create the sprite
            sprite = AnimatedImage(
                spriteName,
                spriteX, spriteY,
                self.SPRITEIMAGES
            )
            sprite.updateImage(
                sprite.getSequence(spriteDict['direction']),
                sprite.getFrame(spriteDict['step']),
                spriteDict
            )
            drawNewSprites.add(sprite)
            self.dirtySprites.add(sprite)
        self.dirtySprites.clear(self.DISPLAYSURF, self.BACKGROUNDSURF)
        spriteList = self.dirtySprites.draw(self.DISPLAYSURF)
        for spriteRect in spriteList:
            self.dirtyRects.append(spriteRect)
        self.dirtySprites.remove(drawNewSprites)

    def setFont(self, fontname, fontsize):
        self.FONTSIZE = fontsize
        self.FONTNAME = fontname

    def getTextSurf(self, text, textcolor, fontsize, fontname):
        """Return a surface containing text.

        This is only to be used internally by Graphics to construct
        lines of text."""
        if fontname == DEFAULT:
            font = pygame.font.SysFont(FONTNAME, fontsize, True, False)
        else:
            font = pygame.font.Font(fontname, fontsize)
        textSurf = font.render(text, True, textcolor)
        textRect = textSurf.get_rect()
        return textSurf, textRect

    def showTextCentered(self, text, centerx, centery,
                         fontcolor=NORMALFONTCOLOR,
                         fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(
            text, fontcolor, fontsize, fontname
        )
        textRect.center = (centerx, centery)
        self.DISPLAYSURF.blit(textSurf, textRect)
        self.addDirtyRect(textRect)

    def showTextBox(self, text, textcolor, boxcolor,
                    x, y, width, height,
                    fontsize=FONTSIZE, fontname=FONTNAME, foreground=False):
        textSurf, textRect = self.getTextSurf(
            text, textcolor, fontsize, fontname)
        boxRect = pygame.Rect((x, y, width, height))
        textRect.topleft = (x, y)
        boxRect.topleft = (x, y)
        pygame.draw.rect(self.DISPLAYSURF, boxcolor, boxRect, 0)
        self.DISPLAYSURF.blit(textSurf, textRect)
        if foreground:
            pygame.draw.rect(self.FOREGROUNDSURF, boxcolor, boxRect, 0)
            self.FOREGROUNDSURF.blit(textSurf, textRect)
        else:
            pygame.draw.rect(self.BACKGROUNDSURF, boxcolor, boxRect, 0)
            self.BACKGROUNDSURF.blit(textSurf, textRect)
        self.addDirtyRect(boxRect)

    def showTextBoxCentered(self, text, textcolor, boxcolor,
                            centerx, centery, width, height,
                            fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(
            text, textcolor, fontsize, fontname
        )
        boxRect = pygame.Rect((centerx, centery, width, height))
        boxRect.center = (centerx, centery)
        # Draw the box that serves as background to text
        boxSurf = pygame.Surface((width, height))
        pygame.draw.rect(boxSurf, boxcolor, boxRect, 0)
        boxSurf.blit(textSurf, textRect)
#        pygame.draw.rect(self.DISPLAYSURF, boxcolor, boxRect, 0)
#        self.DISPLAYSURF.blit(textSurf, textRect)
#        self.addDirtyRect(boxRect)
        # Make the dynamicImage to handle the text-box
        textBoxImage = pygame.sprite.DirtySprite()
        textBoxImage.rect = boxRect
        textBoxImage.image = boxSurf
        self.dirtyImages.add(textBoxImage)

    def showText(self, list, x, y, xparams={'blank': None,}):
        if 'fontname' in xparams:
            fontname = xparams['fontname']
        else:
            fontname=FONTNAME
        if 'fontcolor' in xparams:
            fontcolor = xparams['fontcolor']
        else:
            fontcolor = NORMALFONTCOLOR
        if 'fontsize' in xparams:
            fontsize = xparams['fontsize']
        else:
            fontsize = NORMALFONTSIZE
        listRect = pygame.Rect((x, y, 1, 1))
        gapY = fontsize+NORMALGAP  # Gap between lines of text
        originY = y-(gapY*(len(list)-1))  # Offset based on lines of text
        if originY < y:
            originY += fontsize/2  # Fixes an offset problem
        for index, item in enumerate(list):
            textSurf, textRect = self.getTextSurf(
                item, fontcolor, fontsize, fontname
            )
            if ALIGNMENT in xparams:
                if xparams[ALIGNMENT] == CENTERED:
                    textRect.center = (x, originY+(gapY*index))
                elif xparams[ALIGNMENT] == MIDRIGHT:
                    textRect.midright = (x, originY+(gapY*index))
                elif xparams[ALIGNMENT] == TOPLEFT:
                    textRect.topleft = (x, originY+(gapY*index))
                elif xparams[ALIGNMENT] == MIDTOP:
                    textRect.midtop = (x, originY+(gapY*index))
                elif xparams[ALIGNMENT] == BOTTOMLEFT:
                    textRect.bottomleft = (x, originY+(gapY*index))
            else:
                textRect.topleft = (x, y+((fontsize+NORMALGAP)*index))
            if self.DISPLAYSURF.get_rect().colliderect(textRect):
                if 'blank' in xparams and xparams['blank'] == True:
                    pygame.draw.rect(self.DISPLAYSURF, BLACK, textRect, 0)
                self.DISPLAYSURF.blit(textSurf, textRect)
                if 'foreground' in xparams and xparams['foreground']:
                    self.FOREGROUNDSURF.blit(textSurf, textRect)
                else:
                    self.BACKGROUNDSURF.blit(textSurf, textRect)
                listRect = listRect.union(textRect)
        # Add the whole list as one dirty-rect
        self.addDirtyRect(listRect)

    def addText(self, textlist, alignment, xargs={}):
        if 'fontcolor' in xargs:
            fontColor = xargs['fontcolor']
        else:
            fontColor = NORMALFONTCOLOR
        if 'fontsize' in xargs:
            fontSize = xargs['fontsize']
        else:
            fontSize = NORMALFONTSIZE
        if 'fontname' in xargs:
            fontName = xargs['fontname']
        else:
            fontName = FONTNAME
        container = self.DISPLAYSURF.get_rect()
        textImage = TextImage(textlist, fontSize, fontColor, fontName,
                              container, alignment)
        self.dirtyImages.add(textImage)
        return textImage

    def addImage(self, filename, alignment, xargs={}):
        imageSurf = pygame.image.load(filename)
        imageRect = pygame.Rect(imageSurf.get_rect())
        container = self.DISPLAYSURF.get_rect()
        staticImage = StaticImage(
            imageSurf, imageRect,
            container, alignment
        )
        self.dirtyImages.add(staticImage)

    def showImage(self, filename, x, y, xparams={}):
        imageSurf = pygame.image.load(filename)
        if SCALE in xparams and xparams[SCALE] > 0:
            scale = xparams[SCALE]
            imageW = imageSurf.get_width()
            imageH = imageSurf.get_height()
            imageSurf = pygame.transform.scale(
                imageSurf, (imageW*scale, imageH*scale)
            )
#            tileWidth *= scale
#            tileHeight *= scale
        imageRect = pygame.Rect(imageSurf.get_rect())
        if ALIGNMENT in xparams:
            if xparams[ALIGNMENT] == CENTERED:
                imageRect.center = (x, y)
            elif xparams[ALIGNMENT] == MIDRIGHT:
                imageRect.midright = (x, y)
            elif xparams[ALIGNMENT] == TOPLEFT:
                imageRect.topleft = (x, y)
            elif xparams[ALIGNMENT] == MIDTOP:
                imageRect.midtop = (x, y)
            elif xparams[ALIGNMENT] == BOTTOMLEFT:
                imageRect.bottomleft = (x, y)
        else:
            imageRect.topleft = (x, y)
        self.DISPLAYSURF.blit(imageSurf, imageRect)
        self.dirtyRects.append(imageRect)
        if 'foreground' in xparams and xparams['foreground']:
            self.FOREGROUNDSURF.blit(imageSurf, imageRect)

    def showImageCentered(self, filename, x, y):
        imageSurf = pygame.image.load(filename)
        imageRect = pygame.Rect(imageSurf.get_rect())
        imageRect.center = (x, y)
        self.DISPLAYSURF.blit(imageSurf, imageRect)
        self.dirtyRects.append(imageRect)

    def drawScreen(self, screenparts):
        if 'maps' in screenparts:
            for map in screenparts['maps']:
                self.drawTileMap(map['name'], map['rect'],
                                 map['map'], map['layer'])
        if 'images' in screenparts:
            for img in screenparts['images']:
                self.showImage(img['filename'], img['x'], img['y'],
                               img['xparams'])
        if 'texts' in screenparts:
            for text in screenparts['texts']:
                self.showText(text['list'], text['x'], text['y'],
                              text['xparams'])

    def addDirtyRect(self, rect):
        newDirtyRect = rect
        # Iterate in reverse since we'll be removing items
        for i in range(len(self.dirtyRects)-1, -1, -1):
            if newDirtyRect.colliderect(self.dirtyRects[i]):
                newDirtyRect = newDirtyRect.union(self.dirtyRects.pop(i))
        self.dirtyRects.append(newDirtyRect)

    def updateScreen(self):
        self.dirtyImages.clear(self.DISPLAYSURF, self.BACKGROUNDSURF)
        moreDirtyRects = self.dirtyImages.draw(self.DISPLAYSURF)
        if self.DEBUGMODE:
            print("Dirtyrects: " + str(len(self.dirtyRects)))
            print(self.dirtyRects)
            print("moreDirtyRects: " + str(len(moreDirtyRects)))
        pygame.display.update(self.dirtyRects)
        pygame.display.update(moreDirtyRects)
        self.dirtyRects = []

    def blank(self, bgcolor=BLACK):
        self.DISPLAYSURF.fill(bgcolor)
        self.BACKGROUNDSURF = self.DISPLAYSURF.copy()
        self.FOREGROUNDSURF = pygame.Surface([self.WINDOWWIDTH, self.WINDOWHEIGHT], pygame.SRCALPHA, 32)
        self.textRects = {}
        self.addDirtyRect(self.DISPLAYSURF.get_rect())

    def hide(self):
        self.dirtyImages.empty()
        self.blank()

    def wallMaze(self, level, wallmaze):
        wallmap = wallmaze.wallmap
        wallcolor = GRAY
        pygame.draw.rect(self.DISPLAYSURF, wallcolor, (0, 0, self.WINDOWWIDTH, self.WINDOWHEIGHT), 1)
        for cy in range(len(wallmap)):
            for cx in range(len(wallmap[cy])):
                cellrect = wallmaze.getCellRect(cx, cy)
                if wallmap[cy][cx] & WEST:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.top, 1, cellrect.height), 1)
                if wallmap[cy][cx] & NORTH:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.top, cellrect.width, 1), 1)
                if wallmap[cy][cx] & SOUTH:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.bottom-1, cellrect.width, 1), 1)
                if wallmap[cy][cx] & EAST:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.right-1, cellrect.top, 1, cellrect.height), 1)

    def drawSurface(self, surface, x, y):
        imageSurf = surface
        imageRect = pygame.Rect(imageSurf.get_rect())
        imageRect.topleft = (x, y)
        self.DISPLAYSURF.blit(imageSurf, imageRect)
        self.dirtyRects.append(imageRect)


class ImageSprite(pygame.sprite.DirtySprite):
    def __init__(self, image, rect):
        super(ImageSprite, self).__init__()
        self.image = image
        self.rect = rect
        self.TOPLEFT = TOPLEFT
        self.BOTTOMLEFT = BOTTOMLEFT
        self.TOPRIGHT = TOPRIGHT
        self.BOTTOMRIGHT = BOTTOMRIGHT
        self.MIDTOP = MIDTOP
        self.MIDLEFT = MIDLEFT
        self.MIDBOTTOM = MIDBOTTOM
        self.MIDRIGHT = MIDRIGHT
        self.CENTER = CENTER
        self.xparams = {}


class StaticImage(pygame.sprite.DirtySprite):
    def __init__(self, image, rect, container, alignment):
        super(StaticImage, self).__init__()
        self.TOPLEFT = TOPLEFT
        self.BOTTOMLEFT = BOTTOMLEFT
        self.TOPRIGHT = TOPRIGHT
        self.BOTTOMRIGHT = BOTTOMRIGHT
        self.MIDTOP = MIDTOP
        self.MIDLEFT = MIDLEFT
        self.MIDBOTTOM = MIDBOTTOM
        self.MIDRIGHT = MIDRIGHT
        self.CENTER = CENTER
        self.image = image
        self.rect = rect
        self.rect = self.getAlignedRect(container, alignment)

    def getAlignedRect(self, container, alignment):
        alignedRect = copy.copy(self.rect)
        if alignment == self.BOTTOMLEFT:
            alignedRect.left = container.left
            alignedRect.bottom = container.bottom
        elif alignment == self.TOPRIGHT:
            alignedRect.right = container.right
            alignedRect.top = container.top
        elif alignment == self.BOTTOMRIGHT:
            alignedRect.right = container.right
            alignedRect.bottom = container.bottom
        elif alignment == self.MIDTOP:
            alignedRect.centerx = container.centerx
            alignedRect.top = container.top
        elif alignment == self.MIDBOTTOM:
            alignedRect.centerx = container.centerx
            alignedRect.bottom = container.bottom
        elif alignment == self.CENTER:
            alignedRect.centerx = container.centerx
            alignedRect.centery = container.centery
        return alignedRect

    def changeText(self, newtext):
        textSurf, textRect = self.getTextSurf(
            newtext, self.fontSize, self.fontColor, self.fontName
        )
        self.image = textSurf
        self.dirty = 1

    def getTextSurf(self, textlist, fontsize, fontcolor, fontname):
        """Return a surface containing text."""
        font = pygame.font.Font(fontname, fontsize)
        lineHeight = font.get_linesize()
        lineWidth = 1
        for text in textlist:
            width = font.size(text)[0]
            if width > lineWidth:
                lineWidth = width
        totalSurf = pygame.Surface(
            (lineWidth, lineHeight * len(textlist)),
        )
        totalRect = totalSurf.get_rect()
        totalSurf.set_colorkey(BLACK)
        for line, text in enumerate(textlist):
            textSurf = font.render(text, True, fontcolor)
            textRect = textSurf.get_rect()
            textRect.y = (line * lineHeight)
            totalSurf.blit(textSurf, textRect)
        return totalSurf, totalRect


class TextImage(StaticImage):
    def __init__(self, textlist, fontsize, fontcolor, fontname,
                 container, alignment):
        textSurf, textRect = self.getTextSurf(
            textlist, fontsize, fontcolor, fontname
        )
        # TODO: really messy; figure out the what StaticImage and TextImage
        # individually need as classes, TextImage inherent
        super(TextImage, self).__init__(textSurf, textRect,
                                        container, alignment)
        self.rect = self.getAlignedRect(container, alignment)
        self.fontSize = fontsize
        self.fontColor = fontcolor
        self.fontName = fontname


class AnimatedImage(ImageSprite):
    def __init__(self, rect, image):
        super(AnimatedImage, self).__init__(image, rect)

    def setImageSheet(self, name, images):
        # Find an image sheet based on name
        if name in images:
            imagesDict = images[name]
            imageSheet = imagesDict['image']
            imageWidth = imagesDict['clipwidth']
            imageHeight = imagesDict['clipheight']
            imageSteps = imagesDict['clipsteps']
        # Create a dummy image if not found
        else:
            imageWidth = 8
            imageHeight = 8
            imageSheet = pygame.Surface(
                (imageWidth, imageHeight)
            )
            pygame.draw.rect(
                imageSheet, WHITE,
                (0, 0, imageWidth, imageHeight),
                1
            )
            imageSteps = 1
        self.steps = imageSteps
        self.imageSheet = imageSheet
        return imageWidth, imageHeight

    def getSequence(self, direction):
        """Return coordinates of row of animations to use.

        Tries to use a sequence of animations representing the direction
        the object is facing."""
        height = self.rect.height
        sequences = self.imageSheet.get_height() / height
        sequence = direction / (360/sequences) + 1
        if sequence > sequences:
            sequence = 1
        if (sequence*height) + height > self.imageSheet.get_height():
            sequence = 0
        else:
            if sequence >= sequences:
                sequence = 0
        return sequence

    def getFrame(self, step):
        """Return coordinates of frame of animation to use.

        Stretches the use of each frame to match the number of game
        cycles expected for the animation."""
        frame = step
        if frame > 0:
            frame -= 1
        frames = self.imageSheet.get_width() / self.rect.width
        stepsPerFrame = float(float(self.steps) / float(frames))
        if stepsPerFrame > 0:
            frame = int(frame / stepsPerFrame)
        else:
            frame = 0
        return frame

    def getImageClip(self, sequence, frame, width, height):
        x = (width*frame)
        y = (height*sequence)
        clipRect = (x, y, width, height)
#        if (width*frame)+width > self.imageSheet.get_width():
#            print(str((width*frame)+width) + " exceeded " + str(self.imageSheet.get_width()))
#            x = 0
#        else:
#            x = width*frame
#        if (height*sequence)+height > self.imageSheet.get_height():
#            y = 0
#        else:
#            y = height*sequence
        if self.imageSheet.get_rect().contains(clipRect):
            clipSurf = self.imageSheet.subsurface(clipRect)
        else:
#            print("clip" + str(clipRect) + " exceeded surface " + str(self.imageSheet.get_rect()))
            clipRect = (0, 0, width, height)
        if self.imageSheet.get_rect().contains(clipRect):
            clipSurf = self.imageSheet.subsurface(clipRect)
        else:
            clipSurf = self.imageSheet
        return clipSurf

    def updateImage(self, sequence, frame, xargs={}):
        width = self.rect.width
        height = self.rect.height
        #if "newimagesheet" in xargs:
#        print(sequence, frame)
        self.image = self.getImageClip(sequence, frame, width, height)

    def setObject(self, object):
        self.object = object

    def update(self, imagesdict):
        #frame = self.getFrame(self.object.step)
        frame = 0
        if self.object.status not in (None, 'None'):
            spritename = self.object.name + "_" + self.object.status
        else:
            spritename = self.object.name
        if spritename in imagesdict:
            imageDict = imagesdict[spritename]
            self.imageSheet = imageDict['image']
            imageWidth = imageDict['clipwidth']
            imageHeight = imageDict['clipheight']
            stepsPerFrame = imageDict['steps']
            totalFrames = self.imageSheet.get_width()/imageWidth
            if 'sequence' in imageDict:
                frameIndex = int(self.object.step/stepsPerFrame)
                if frameIndex <= len(imageDict['sequence']):
                    imageFrame = imageDict['sequence'][frameIndex-1]
                else:
                    imageFrame = 0
                if imageFrame >= totalFrames:
                    imageFrame = totalFrames-1
            else:
                imageFrame = int(self.object.step/stepsPerFrame)
                if imageFrame >= totalFrames:
                    imageFrame = totalFrames-1
            imageSeq = 0
            imageFlip = None
            imageRot = 0
            if 'fliph' not in self.xparams:
                if 'directions' in imageDict:
                    for i, dir in enumerate(imageDict['directions']):
                        if self.object.direction == dir:
                            imageSeq = i
                            imageFlip = False
                if 'flips' in imageDict:
                    for i, dir in enumerate(imageDict['flips']):
                        if self.object.direction == dir:
                            imageSeq = i
                            imageFlip = True
                if 'rots' in imageDict:
                    for i, dir in enumerate(imageDict['rots']):
                        if self.object.direction == dir:
                            imageSeq = i
                            imageRot = 90
                if 'durots' in imageDict:
                    for i, dir in enumerate(imageDict['durots']):
                        if self.object.direction == dir:
                            imageSeq = i
                            imageRot = 180
                if 'trirots' in imageDict:
                    for i, dir in enumerate(imageDict['trirots']):
                        if self.object.direction == dir:
                            imageSeq = i
                            imageRot = 270
                direction = self.object.direction + 180
                if direction > 360:
                    direction -= 360
                imageDirect = (direction/90)-1
            else:
                imageDirect = 0
            image = self.getImageClip(
                imageSeq, imageFrame,
                imageWidth, imageHeight,
#                directPolicy
            )
            if imageFlip:
                image = pygame.transform.flip(image, True, False)
            if imageRot:
                image = pygame.transform.rotate(image, imageRot)
            if 'fliph' in self.xparams:
                if self.object.direction == self.xparams['fliph']:
                    image = pygame.transform.flip(image, True, False)
            self.image = image
            # resize and align the rectangle for rendering
            self.rect.width = self.image.get_width()
            self.rect.height = self.image.get_height()
            if 'alignment' in imageDict:
                if imageDict['alignment'] == 'bottom':
                    self.rect.centerx = self.object.centerx
                    self.rect.bottom = self.object.bottom
            else:
                self.rect.center = self.object.center
        else:
            self.rect.center = self.object.center
        # Make this sprite invisible if object is "killed"
        if self.object.killed:
            self._set_visible(0)
        else:
            self._set_visible(1)
