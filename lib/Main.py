from ebigo import metagame

import templeObjects

class Meta(metagame.MetaGame):
    def __init__(self, winwidth, winheight):
        game = Game(winwidth, winheight)
        super(Meta, self).__init__(game, winwidth, winheight)
        self.FPS = 30

    def getSpriteSets(self):
        return (
            {'name': "NinjaF", 'width': 16, 'height': 18,
             'alignment': 'bottom', 'steps': 3,
             'sequence': (1,0,1,2),
             'directions': (270,180,90,360)},
            {'name': "RangerM", 'width': 16, 'height': 18,
             'alignment': 'bottom', 'steps': 3,
             'sequence': (1,0,1,2),
             'directions': (270,180,90,360)},
            {'name': "slime", 'width': 16, 'height': 16,
             'sequence': (0,1,0,2),
             'alignment': 'bottom', 'steps': 4,},
            {'name': "mushroom", 'width': 16, 'height': 16,
             'alignment': 'bottom', 'steps': 4,
             'directions': (90,270,180,360),},
            {'name': "bolt", 'width': 13, 'height': 13,
             'steps': 4, 'directions': (360,),
             'rots': (90,), 'durots': (180,), 'trirots': (270,),},
            {'name': "star", 'width': 15, 'height': 15, 'steps': 4,},
        )

    def getTileSets(self):
        field = self.game.getAllFieldObjs()[0]
        boxmaps = field['maps']
        return (
            {'name': "dungeon2", 'maps': boxmaps,
             'tilewidth': 16, 'tileheight': 16},
        )


class Game(templeObjects.Game):
    def __init__(self, winwidth, winheight):
        super(Game, self).__init__()
        self.scale = 2
        self.playField = self.addFieldObject(
            templeObjects.Room(self.scale)
        )
        self.PLAYER1 = 0
        self.PLAYER2 = 1
        self.addPlayer("NinjaF", self.PLAYER1, self.playField.PBOUNDRECT)
        self.addPlayer("RangerM", self.PLAYER2, self.playField.PBOUNDRECT)
        self.fillRoom()

    def cleanRoom(self):
        for monster in self.monsters:
            self.removeObject(monster)
        self.monsters = []

    def fillRoom(self):
        self.addMonsters(6, self.playField.MBOUNDRECT)

    def playGame(self, controldicts):
        field = self.getField()
        super(Game, self).playGame(controldicts)
        # TODO: collisions aren't this complicated. Fix it!
        layer = 0
        for object in self.allObjs:
            direct = object.direction
            blockList = field.getBlocksAtRect(
                object, self.blockBox, direct, layer
            )
            if object.moveOutsideBlocks(blockList):
                object.hitBlock()
        for monster in self.monsters:
            monster.wander()
        # resolve missile collisions
        self.doCombat()
        # move to a new room if player exits
        for player in self.players:
            if player.exitRoom:
                self.cleanRoom()
                self.fillRoom()
                player.exitRoom = False
                return True
